<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if IE 9]>         <html class="no-js ie9"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js"> <!--<![endif]-->
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>StudioUm</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,400,300,600,700' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Oswald' rel='stylesheet' type='text/css'>
		<script src="js/modernizr.custom.js" type="text/javascript"></script>
	<link rel="stylesheet" href="bower_components/magnific-popup/dist/magnific-popup.css">
	<link rel="stylesheet" href="bower_components/slick-carousel/slick/slick.css">
	<link rel="stylesheet" href="css/style.css">
	<!--[if lt IE 9]>
		<script src="bower_components/html5shiv/dist/html5shiv.min.js" type="text/javascript"></script>
		<![endif]-->
</head>
<body>
<div class="loading-mask"></div>
	<div class="wrapper" id="intro">
			<nav id="main-nav" class="main-nav before-breakpoint">
				<div class="nav-bar container">
					<a href="#" title="Menu" class="open-menu"><span class="menu-icon"></span>Menu</a>
					<h1 class="logo"><a href="#intro" title="StudioUm" data-action="go-to">StudioUm</a></h1>
					<a href="#contact" title="Contato" class="menu-contact" data-action="go-to">Contato</a>
				</div>
				<div class="nav-menu">
					<ul class="menu">
						<li class="menu-item"><a href="#intro" title="" class="close close-menu" data-action="go-to">fechar</a></li>
						<li class="menu-item"><a href="#intro" title="" class="menu-link" data-action="go-to">Home</a></li>
						<li class="menu-item"><a href="#about" title="" class="menu-link" data-action="go-to">Sobre</a></li>
						<li class="menu-item"><a href="#services" title="" class="menu-link" data-action="go-to">Serviços</a></li>
						<li class="menu-item"><a href="#portfolio" title="" class="menu-link" data-action="go-to">Portfólio</a></li>
						<li class="menu-item"><a href="#contact" title="" class="menu-link" data-action="go-to">Contato</a></li>
					</ul>
				</div>
			</nav>
		<header class="main-header">
			<div class="slides">
				<div class="slide" style="background: url(images/temp/slide.jpg) center top no-repeat; background-size: cover;">
					<div class="slide-container clearfix">
						<canvas class="triangles"></canvas>
						<div class="triangle-img triangle-1"></div>
						<div class="triangle-img triangle-2"></div>
						<p class="slide-about">projeto fachada Brookfield</p>
						<h2 class="slide-title">Um novo conceito em projetos arquitetônicos</h2>
						<a href="#" title="" class="view-project">ver projeto</a>
					</div>
				</div>
				<div class="slide" style="background: url(images/temp/slide.jpg) center top no-repeat; background-size: cover;">
					<div class="slide-container clearfix">
						<canvas class="triangles"></canvas>
						<div class="triangle-img triangle-1"></div>
						<div class="triangle-img triangle-2"></div>
						<p class="slide-about">projeto fachada Ecovillaggio</p>
						<h2 class="slide-title">Um novo conceito em projetos arquitetônicos</h2>
						<a href="#" title="" class="view-project">ver projeto</a>
					</div>
				</div>
				<div class="slide" style="background: url(images/temp/slide.jpg) center top no-repeat; background-size: cover;">
					<div class="slide-container clearfix">
						<canvas class="triangles"></canvas>
						<div class="triangle-img triangle-1"></div>
						<div class="triangle-img triangle-2"></div>
						<p class="slide-about">projeto fachada Rio Colorado</p>
						<h2 class="slide-title">Um novo conceito em projetos arquitetônicos</h2>
						<a href="#" title="" class="view-project">ver projeto</a>
					</div>
				</div>
				<div class="slide" style="background: url(images/temp/slide.jpg) center top no-repeat; background-size: cover;">
					<div class="slide-container clearfix">
						<canvas class="triangles"></canvas>
						<div class="triangle-img triangle-1"></div>
						<div class="triangle-img triangle-2"></div>
						<p class="slide-about">projeto fachada Vivaldi</p>
						<h2 class="slide-title">Um novo conceito em projetos arquitetônicos</h2>
						<a href="#" title="" class="view-project">ver projeto</a>
					</div>
				</div>
			</div>
		</header><!-- /header -->
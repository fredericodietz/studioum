jQuery(document).ready(function($) {
	"use strict";

	var mainNav = $("#main-nav"),
	body = $("body");

	var breakpoint = $(window).height() + mainNav.height();


	function changeMenuBehaviour() {
    if($(window).width() > 768) {
      $(window).scroll(function(event) {
        if($(window).scrollTop() > breakpoint) {
          mainNav.addClass('sticky');
        } else {
          mainNav.removeClass('sticky');
        }
      });
    } else {
      var didScroll,
      lastScrollTop = 0,
      delta = 5,
      navbarHeight = mainNav.outerHeight();

      $(window).scroll(function(event){
        didScroll = true;
      });

      setInterval(function() {
        if (didScroll) {
         hasScrolled();
         didScroll = false;
       }
     }, 250);
    }

    function hasScrolled() {
     var st = $(window).scrollTop();

     if(Math.abs(lastScrollTop - st) <= delta)
      return;

    if(st < breakpoint) {
      mainNav.removeClass('sticky');
      mainNav.addClass('before-breakpoint');
    } else {
      mainNav.removeClass('before-breakpoint');
      if (st > lastScrollTop && st > navbarHeight){
	        // Scroll Down
	        mainNav.removeClass('sticky');
	      } else {
	        // Scroll Up
	        if(st + $(window).height() < $(document).height()) {
	        	mainNav.addClass('sticky');
	        }
	      }
	    }
	    lastScrollTop = st;
	  }
	}

	changeMenuBehaviour();

	window.addEventListener("resize", changeMenuBehaviour);


  $(".open-menu, .close-menu").click(function(e){
  	e.preventDefault();
  	$("#main-nav").toggleClass("open");
  });

  $(".open-cat-menu").click(function(e){
  	e.preventDefault();
  	$(this).toggleClass("is-open");
  });

  $('.slides').slick({
  	slidesToShow: 1,
  	slidesToScroll: 1,
  	arrows: false,
  	dots: true,
  	autoplay: true,
  	autoplaySpeed: 3000,
  	responsive: [
  	{
  		breakpoint: 700,
  		settings: {
  			dots: false
  		}
  	}
  	]
  });

  $('.clients-carousel').slick({
  	slidesToShow: 6,
  	slidesToScroll: 6,
  	arrows: false,
  	dots: true,
  	responsive: [
  	{
  		breakpoint: 800,
  		settings: {
  			slidesToShow: 4,
  			slidesToScroll: 4,
  			infinite: true,
  			dots: true
  		}
  	},
  	{
  		breakpoint: 600,
  		settings: {
  			slidesToShow: 3,
  			slidesToScroll: 3,
  			arrows: true,
  			dots: false
  		}
  	},
  	{
  		breakpoint: 550,
  		settings: {
  			slidesToShow: 2,
  			slidesToScroll: 2,
  			arrows: true,
  			dots: false
  		}
  	},
  	{
  		breakpoint: 480,
  		settings: {
  			slidesToShow: 1,
  			slidesToScroll: 1,
  			arrows: true,
  			dots: false
  		}
  	}
  	]
  });

  $(".service-item").css({
  	height: (window.innerHeight/2 - 85) + "px"
  });

  $(".service-content .close").click(function(e){
  	e.preventDefault();
  	var opened = $(this).parent();

  	opened.removeClass("opened");
  	$(".services-items").css({
  		height: ""
  	});
  	// $(".services").css({
  	// 	zIndex: ""
  	// });
  });

  $(".service-link").click(function(e){
  	e.preventDefault();
  	var target = $($(this).attr("href"));
  	$(".opened").removeClass("opened");
  	target.addClass("opened");

  	$("body, html").animate({
  		scrollTop: target.offset().top - 60
  	}, 500);
  	$(".services-items").css({
  		height: target.outerHeight() + "px"
  	});
  	// $(".services").css({
  	// 	zIndex: -1
  	// });
  });

  function showPortfolioItem(data) {
  	$(".portfolio-content").html(data);
  	$("body").css("overflow-y", "hidden");

		// Carrega tabs do portfólio
		displayTabs();

		$('.gallery-carousel').slick({
			arrows: true,
			infinite: false,
			variableWidth: true,
			dots: false
		});

    $('.gallery-carousel').each(function() {
      $(this).magnificPopup({
        delegate: 'a',
        type: 'image',
        gallery: {
          enabled:true,
          tCounter: '%curr% de %total%'
        }
      });
    });

    setTimeout(function() {
      $(".loading-mask").removeClass("is-visible");
      $(".main").css({
        opacity: 0
      });
      $(".main-footer").addClass('to-front');
    }, 600);
    mainNav.addClass('sticky');
  }

  $("body").on("click", ".portfolio-item, .view-project", function(e){
    e.preventDefault();
    var url = $(this).attr("href");
    $(".cat-menu-item--current").removeClass("cat-menu-item--current");
    $(".main-nav").addClass('sticky');
    $(".loading-mask").addClass("is-visible");
    $(".portfolio-content").empty();
    $(".portfolio-lightbox").addClass("show").css({
      height: window.innerHeight + "px"
    }).scrollTop(0);

    $.ajax({
      url: url,
      type: 'GET',
    })
    .done(function(data) {
      setTimeout(showPortfolioItem(data), 600);
    })
    .fail(function() {
      showPortfolioItem("<article class='container portfolio-error'><h2>Nenhum item encontrado.</h2></article><a href='#' class='back'>voltar para portfólio</a>");
    });
  });

  function closePortfolioItem(event) {
    event.preventDefault();
    $(".portfolio-lightbox").animate({
      scrollTop: 0
    }, 700, function(){
      $(".portfolio-lightbox").removeClass("show").css({
        height: "0px"
      });
      $(".main").css({
        opacity: 1
      });
      $(".main-footer").removeClass('to-front');
      $("body").css("overflow-y", "scroll");
    });
  }

  $(".portfolio-content").on("click", ".back", closePortfolioItem);

  var portfolioMasonry = $('.portfolio-items');
  portfolioMasonry.masonry({
    columnWidth: ".portfolio-item",
    itemSelector: '.portfolio-item'
  });

  $(".cat-menu-link").click(function(e){
    e.preventDefault();
    var current = $(".cat-menu-item--current"),
    parent = $(this).parent();
    if(!(parent.hasClass("cat-menu-item--current"))) {
     current.removeClass("cat-menu-item--current");
     parent.addClass( "cat-menu-item--current" );
   } else {
     current.removeClass("cat-menu-item--current");
   }
 });

	// Filtra items do portfólio
	var cache = [],
	items;
	portfolioMasonry.find(".portfolio-item").each(function(){
		cache.push($(this));
	});

	var filterItems = function(selector){
		var result=[];
		$(cache).each(function(item){
			if(this.hasClass(selector)) {
				if($.inArray(cache[item], result) === -1) result.push(cache[item]);
			}
		});
		return result;
	};

	$(".cat-menu-item a").click(function(e){
		e.preventDefault();
		$(".touch body, html.touch").animate({
			scrollTop: $(".portfolio-items").offset().top
		}, 500);
		if($(this).hasClass('submenu-link')) {
			$(".cat-menu-item--current").removeClass("cat-menu-item--current");
		}
		var category = $(this).data("cat");
		portfolioMasonry.empty();
		if(category === "todos") {
			cache.forEach(function(item){
				portfolioMasonry.append(item);
			});
			portfolioMasonry.masonry('reloadItems');
			portfolioMasonry.masonry();
		} else {
			items = filterItems("cat-" + category);
			if(items.length === 0) {
				portfolioMasonry.append("<p>Nenhum item encontrado!</p>");
				return;
			}
			$(items).each(function(){
				portfolioMasonry.append($(this));
			});
			portfolioMasonry.masonry('reloadItems');
			portfolioMasonry.masonry();

		}
	});


	//
  // Scroll até o destino do link
  //
  // Ex.:
  // <a href="#target" data-action="go-to"></a>
  //
  // <h2 id="target">Target Example</h2>
  //

  $(".menu-link, [data-action=go-to]").on("click touchend", function(event) {
  	event.preventDefault();
  	var el     = $(this),
  	target = el.attr("href");
  	closePortfolioItem(event);
  	$("#main-nav").removeClass("open");
  	$("body, html").animate({
  		scrollTop: $(target).offset().top
  	}, 500);
  });

  //
	// Tabs
	//
	// <ul class="tabs" data-target="#container">
	// 	<li class="tab-item">
	// 		<a href="#target" data-action="tab"></a>
	// 	</li>
	// </ul>
	// <div class="tabs-container" id="container">
	// 	<div id="target"></div>
	// </div>
	//

	function displayTabs() {
		$(".tabs-container > div").hide();
		$(".tabs-container > div:first-child").show().addClass('current');
		$(".tabs li:first-child").addClass('active');
		$("[data-action=tab]").click(function(e){
			e.preventDefault();
			var el 		 = $(this),
			target = el.attr("href"),
			container = el.parents(".tabs").data("target");
			if(("#" + $(".current").attr("id")) != target){
				el.parents(".tabs").find(".active").removeClass("active");
				el.parent().addClass("active");
				$(container).find(".current").removeClass("current").fadeOut(200, function(){
					$(target).fadeIn(300, function(){
						$(this).addClass("current");
					});
				});
				$('.gallery-carousel').slick();
			}
		});
	}

	$('.bgParallax').each(function(){
		var $obj = $(this),
		offsetTop = this.offsetTop + $(window).height();

		$(window).scroll(function() {
			if($(window).scrollTop() >= offsetTop - $(window).height()) {
				var yPos = -($(window).scrollTop() / $obj.data('speed')) + offsetTop;
				var bgpos = '50% '+ yPos + 'px';
				$obj.css('background-position', bgpos );
			}
		});
	});

	$('.telefone input').mask('(00) 0000-0000', {placeholder: "(__) ____-____"});

	function drawTriangles(canvas){
		var ctx = canvas.getContext('2d'),
		triangles = 2,
		w = window.innerWidth,
		h = window.innerHeight;

		canvas.style.width = w + "px";
		canvas.style.height= h + "px";
		canvas.width = w;
		canvas.height = h;

		function rnd(m, ma) {
			return Math.floor( (Math.random() * (ma - m + 1) ) + m);
		}
		var start = 0,
		finish = w/triangles;

		for (var i = 0; i < triangles; i++) {
			ctx.beginPath();
			ctx.moveTo(rnd(start, finish), rnd(0, h/2));
			ctx.lineTo(rnd(start, finish), rnd(0, h/2));
			ctx.lineTo(rnd(start, finish), rnd(0, h/2));
			ctx.fillStyle = 'rgba(199, 193, 140, 0.5)';
			ctx.closePath();

			ctx.fill();
			start = w/triangles;
			finish = w;
		}
	}

	if(Modernizr.canvas) {
		var canvas = document.querySelectorAll('.triangles');
		for (var i = canvas.length - 1; i >= 0; i--) {
			drawTriangles(canvas[i]);
		}
	} else {
		$(".triangle-img").show();
	}

	$("form .button").click(function(){
		$("body, html").animate({
			scrollTop: $("form").offset().top - 100
		}, 500);
	});

});

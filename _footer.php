<footer class="main-footer footer clearfix">
	<div class="container">
		<div class="footer-info">
			<h2 class="sub-title">Studio Um Arquitetura</h2>
			<p>Somos um escritório de arquitetura em Goiânia com empenho em analisar a consolidação das estruturas facilita a criação das condições inegavelmente apropriadas.</p>
		</div>
		<div class="footer-logo">
			<img src="images/logo-footer.png" alt="Studio Um">
			<p><small>&copy; 2014 Studio Um Arquitetura</small></p>
		</div>
	</div>
	<a href="http://www.katon.com.br/?utm_source=site%20de%20cliente&utm_medium=assinatura&utm_campaign=studioum" target="_blank" class="assinatura-katon">
		<img src="images/logo-katon.png">
	</a>	
</footer>
</div>
<div class="portfolio-lightbox">
	<div class="portfolio-content clearfix">
		<header class="portfolio-item-header clearfix" style="background: url(images/temp/portfolio-header.jpg) center center no-repeat;">
			<div>
				<h3>Portfolio</h3>
				<h2>Saint Lorence Residence</h2>
				<span class="icon-place">Goiânia - GO</span><span class="icon-category">Residencial</span>	
			</div>	
		</header>
		<article class="container">
			<h3>Sobre este projeto</h3>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rerum ad eveniet laborum sapiente odio dolorem hic perferendis quibusdam. Voluptatibus at, velit exercitationem ipsum quam, earum quia impedit cum totam dicta!</p>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rerum ad eveniet laborum sapiente odio dolorem hic perferendis quibusdam. Voluptatibus at, velit exercitationem ipsum quam, earum quia impedit cum totam dicta!</p>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rerum ad eveniet laborum sapiente odio dolorem hic perferendis quibusdam. Voluptatibus at, velit exercitationem ipsum quam, earum quia impedit cum totam dicta!</p>
			<h4 class="sub-title">Cliente</h4>
			<p>Loft Construtora e Merzian Incorporadora</p>
			<div class="portfolio-item-stats clearfix">
				<div class="one-half">
					<p class="sub-title icon-area">Área <small>(bruta)</small> <span>8000m2</span></p>
					<p class="sub-title icon-area-dashed">Área <small>(unidade)</small> <span>8000m2</span></p>
				</div>
				<div class="one-half">
					<p class="sub-title icon-units">Área <span>8000m2</span></p>
					<p class="sub-title icon-car">Área <span>8000m2</span></p>
				</div>
			</div>
			 <ul class="tabs" data-target="#gallery-container">
			 	<li class="tab-item sub-title">
			 		<a href="#imagens" data-action="tab">Imagens</a>
			 	</li>
			 	<li class="tab-item sub-title">
			 		<a href="#plantas" data-action="tab">Plantas</a>
			 	</li>
			 </ul>
			 <div class="tabs-container" id="gallery-container">
			 	<div id="imagens">
			 		<div class="gallery-carousel">
			 			<div><a href="images/temp/gallery-photo.jpg" title="" rel="prettyPhoto[images]"><img src="images/temp/gallery-photo.jpg" alt=""></a></div>
			 			<div><a href="images/temp/gallery-photo.jpg" title="" rel="prettyPhoto[images]"><img src="images/temp/gallery-photo.jpg" alt=""></a></div>
			 			<div><a href="images/temp/gallery-photo.jpg" title="" rel="prettyPhoto[images]"><img src="images/temp/gallery-photo.jpg" alt=""></a></div>
			 			<div><a href="images/temp/gallery-photo.jpg" title="" rel="prettyPhoto[images]"><img src="images/temp/gallery-photo.jpg" alt=""></a></div>
			 			<div><a href="images/temp/gallery-photo.jpg" title="" rel="prettyPhoto[images]"><img src="images/temp/gallery-photo.jpg" alt=""></a></div>
			 			<div><a href="images/temp/gallery-photo.jpg" title="" rel="prettyPhoto[images]"><img src="images/temp/gallery-photo.jpg" alt=""></a></div>
			 		</div>
			 	</div>
			 	<div id="plantas">
			 		<div class="gallery-carousel">
			 			<div><a href="images/temp/gallery-photo.jpg" title="" rel="prettyPhoto[plantas]"><img src="images/temp/gallery-photo.jpg" alt=""></a></div>
			 			<div><a href="images/temp/gallery-photo.jpg" title="" rel="prettyPhoto[plantas]"><img src="images/temp/gallery-photo.jpg" alt=""></a></div>
			 			<div><a href="images/temp/gallery-photo.jpg" title="" rel="prettyPhoto[plantas]"><img src="images/temp/gallery-photo.jpg" alt=""></a></div>
			 			<div><a href="images/temp/gallery-photo.jpg" title="" rel="prettyPhoto[plantas]"><img src="images/temp/gallery-photo.jpg" alt=""></a></div>
			 			<div><a href="images/temp/gallery-photo.jpg" title="" rel="prettyPhoto[plantas]"><img src="images/temp/gallery-photo.jpg" alt=""></a></div>
			 			<div><a href="images/temp/gallery-photo.jpg" title="" rel="prettyPhoto[plantas]"><img src="images/temp/gallery-photo.jpg" alt=""></a></div>
			 		</div>
			 	</div>
			 </div>	 
		</article>
		<a href="#" class="back">voltar para portfólio</a>
		<div class="clear"></div>
	</div>
</div>
<script src="bower_components/jquery/dist/jquery.js" type="text/javascript"></script>
<script src="bower_components/slick-carousel/slick/slick.js" type="text/javascript"></script>
<script src="bower_components/masonry/dist/masonry.pkgd.js" type="text/javascript"></script>
<script src="bower_components/magnific-popup/dist/jquery.magnific-popup.js" type="text/javascript"></script>
<script src="bower_components/jQuery-Mask-Plugin/dist/jquery.mask.js" type="text/javascript"></script>
<script src="js/main.js"></script>
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>
<script>
	function initialize() {

		var styles = [
		{
			"featureType": "landscape",
			"stylers": [
			{
				"saturation": -100
			},
			{
				"lightness": 65
			},
			{
				"visibility": "on"
			}
			]
		},
		{
			"featureType": "poi",
			"stylers": [
			{
				"saturation": -100
			},
			{
				"lightness": 51
			},
			{
				"visibility": "simplified"
			}
			]
		},
		{
			"featureType": "road.highway",
			"stylers": [
			{
				"saturation": -100
			},
			{
				"visibility": "simplified"
			}
			]
		},
		{
			"featureType": "road.arterial",
			"stylers": [
			{
				"saturation": -100
			},
			{
				"lightness": 30
			},
			{
				"visibility": "on"
			}
			]
		},
		{
			"featureType": "road.local",
			"stylers": [
			{
				"saturation": -100
			},
			{
				"lightness": 40
			},
			{
				"visibility": "on"
			}
			]
		},
		{
			"featureType": "transit",
			"stylers": [
			{
				"saturation": -100
			},
			{
				"visibility": "simplified"
			}
			]
		},
		{
			"featureType": "administrative.province",
			"stylers": [
			{
				"visibility": "off"
			}
			]
		},
		{
			"featureType": "water",
			"elementType": "labels",
			"stylers": [
			{
				"visibility": "on"
			},
			{
				"lightness": -25
			},
			{
				"saturation": -100
			}
			]
		},
		{
			"featureType": "water",
			"elementType": "geometry",
			"stylers": [
			{
				"hue": "#ffff00"
			},
			{
				"lightness": -25
			},
			{
				"saturation": -97
			}
			]
		}
		];

		if(Modernizr.touch && window.innerWidth < 900) {
			var map = document.getElementById('map_canvas');
			var img = document.createElement("img");
			img.src = "https://maps.googleapis.com/maps/api/staticmap?center=-16.7157398,-49.2774558&zoom=13&size=" + window.innerWidth + "x300&maptype=roadmap&markers=color:black%7C-16.713995, -49.287962";
			map.appendChild(img);
		} else {
			var styledMap = new google.maps.StyledMapType(styles,
				{name: "Styled Map"});

			var centerLatLng = new google.maps.LatLng(-16.7157398,-49.2774558);
			var myLatLng = new google.maps.LatLng(-16.713995, -49.287962);
			var mapOptions = {
				zoom: 15,
				scrollwheel: false,
				center: centerLatLng,
				mapTypeControlOptions: {
					mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'map_style']
				}
			};
			var map = new google.maps.Map(document.getElementById('map_canvas'),
				mapOptions);
			var iconBase =  'images/';
			var marker = new google.maps.Marker({
				position: myLatLng,
				map: map,
				icon: iconBase + 'location.png'
			});
			map.mapTypes.set('map_style', styledMap);
			map.setMapTypeId('map_style');
		}

	}
	initialize();
	window.addEventListener("resize", initialize);
</script>
</body>
</html>
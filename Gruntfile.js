module.exports = function(grunt) {
	'use strict';
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		dist: "../site/wp-content/themes/tema",
		watch: {
			options: {
				livereload: true
			},
			css: {
				files: ['src/sass/**'],
				tasks: 'compass:dev'
			},
			js: {
				files: 'js/main.js',
				tasks: 'jshint',
	      options: {
	        spawn: false,
	      }
			},
			php: {
				files: '*.php'
			},
			html: {
				files: '*.html'
			}
		},
		compass: {
			dev: {
				options: {
					sassDir: 'src/sass',
					cssDir: 'css/',
					specify: 'src/sass/style.scss',
					httpImagesPath: '../images/',
					generatedImagesDir: "images/sprites/",
					httpGeneratedImagesPath: "../images/sprites",
					httpFontsPath: "fonts/",
					outputStyle: 'expanded'
				}
			},
			dist: {
				options: {
					banner :
						"/* \n" +
						"Theme Name: <%= pkg.description %>\n" +
						"Version: 1.0\n" +
						"Author: <%= pkg.author %>\n" +
						"Author URI: http://katon.com.br\n" +
						"*/\n",
					sassDir: 'src/sass/',
					cssDir: '<%= dist %>/',
					specify: 'src/sass/style.scss',
					httpGeneratedImagesPath: "images/",
					outputStyle: 'compressed',
					httpImagesPath: 'images/',
					httpFontsPath: "css/fonts/"
				}
			}
		},
		jshint: {
			files: ['Gruntfile.js', 'js/main.js'],
			options: {
				globals: {
					jQuery: true
				}
			}
		},
	  concat: {
      options: {
        separator: ';\n'
      },
      dist: {
        src: ['!js/plugins.js', 'bower_components/slick-carousel/slick/slick.js', 'bower_components/masonry/dist/masonry.pkgd.js', 'bower_components/magnific-popup/dist/jquery.magnific-popup.js', 'bower_components/jQuery-Mask-Plugin/dist/jquery.mask.min.js'],
        dest: 'js/plugins.js'
      }
    },
		uglify: {
			my_target: {
				options: {
					banner: "// <%= pkg.author %> \n"
				},
				files: {
					'js/main.min.js': 'js/main.js',
	  			'js/plugins.min.js': 'js/plugins.js'
				}
			}
		},
	  copy: {
	  	main: {
	  		files: [
		  		{expand: false, src: 'bower_components/html5shiv/dist/html5shiv.min.js', dest: "<%= dist %>/js/html5shiv.min.js"},
		  		{expand: false, src: 'bower_components/slick-carousel/slick/slick.css', dest: "<%= dist %>/css/slick.css"},
		  		{expand: false, src: 'bower_components/magnific-popup/dist/magnific-popup.css', dest: "<%= dist %>/css/magnific-popup.css"}
		  	]
	  	},
	  	images: {
	  		files: [
	  			{expand: true, src: ['images/**', '!images/temp/**'], dest: "<%= dist %>/"},
	  			{expand: true, cwd: "bower_components/jquery-prettyPhoto/images/prettyPhoto/", src: '**', dest: "<%= dist %>/images/prettyPhoto/"}
	  		]
	  	},
	  	js: {
	  		files: [
		  		{expand: true, src: 'js/plugins.min.js', dest: "<%= dist %>/"},
		  		{expand: false, src: 'js/main.min.js', dest: "<%= dist %>/js/main.js"},
		  		{expand: false, src: 'js/modernizr.custom.js', dest: "<%= dist %>/js/modernizr.custom.js"}
	  		]
	  	}
	  }
	});

	grunt.loadNpmTasks('grunt-contrib-compass');
	grunt.loadNpmTasks('grunt-contrib-jshint');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.registerTask('default', ['watch']);
	grunt.registerTask('build', ['compass:dist', 'jshint', 'concat', 'uglify', 'copy:main', 'copy:js']);
	grunt.registerTask('images', ['copy:images']);
	grunt.registerTask('js', ['jshint', 'concat', 'uglify', 'copy:js']);
};
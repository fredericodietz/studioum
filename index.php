<?php include "_header.php" ?>
<div class="clear"></div>
<div class="content main">
<div class="sections-wrapper bgParallax" data-speed="0.7">
	<section class="about" id="about">
		<div class="container clearfix">
			<h2>Somos um escritório de arquitetura em Goiânia com empenho em analisar a consolidação das estruturas facilita a criação das condições inegavelmente apropriadas.</h2>
			<p>Percebemos, cada vez mais, que o acompanhamento das preferências de consumo estende o alcance e a importância dos conhecimentos estratégicos para atingir a excelência.</p>
			<p>Acima de tudo, é fundamental ressaltar que o novo modelo estrutural aqui preconizado afeta positivamente a correta previsão do fluxo de informações.</p>
		</div>
	</section>
	<section class="services clearfix" id="services">
		<h2>Serviços</h2>
		<ul class="services-items clearfix">
			<li class="item">
				<a href="#estudo-de-viabilidade-do-terreno" class="service-link" title="">
					<div class="service-item">
						<div>
							<h3 class="section-title icon-search icon-top">Estudo de viabilidade do terreno</h3>
						</div>
					</div>
				</a>
				<div class="service-content" id="estudo-de-viabilidade-do-terreno">
					<a href="#" title="" class="close">fechar</a>
					<div class="container">
						<h3 class="section-title icon-search icon-left">Estudo de viabilidade do terreno</h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Harum laudantium, soluta ducimus dolorum ipsum hic at? Reiciendis mollitia saepe consequatur, veritatis sed sint. Eveniet sunt, dolore, velit provident voluptates quos.</p>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Temporibus quod ipsa doloremque enim debitis atque dolorum minima numquam labore perferendis, ut veniam nesciunt eligendi eveniet veritatis id magni quis nemo.</p>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Harum laudantium, soluta ducimus dolorum ipsum hic at? Reiciendis mollitia saepe consequatur, veritatis sed sint. Eveniet sunt, dolore, velit provident voluptates quos.</p>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Temporibus quod ipsa doloremque enim debitis atque dolorum minima numquam labore perferendis, ut veniam nesciunt eligendi eveniet veritatis id magni quis nemo.</p>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Harum laudantium, soluta ducimus dolorum ipsum hic at? Reiciendis mollitia saepe consequatur, veritatis sed sint. Eveniet sunt, dolore, velit provident voluptates quos.</p>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Temporibus quod ipsa doloremque enim debitis atque dolorum minima numquam labore perferendis, ut veniam nesciunt eligendi eveniet veritatis id magni quis nemo.</p>
					</div>
				</div>
			</li>
			<li class="item">
				<a href="#projeto-de-arquitetura" class="service-link" title="">
					<div class="service-item">
						<div>
							<h3 class="section-title icon-project icon-top">Projeto de Arquitetura</h3>
						</div>
					</div>
				</a>
				<div class="service-content" id="projeto-de-arquitetura">
					<a href="#" title="" class="close">fechar</a>
					<div class="container">
						<h3 class="section-title icon-project icon-left">Projeto de Arquitetura</h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tempore numquam modi, aut nulla quod praesentium. Nesciunt magnam, fugiat deserunt esse. Molestiae ad, repellendus. Quibusdam minima ea velit, corrupti voluptates rerum!</p>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci deleniti laudantium qui accusantium ex, quibusdam beatae, nesciunt necessitatibus sapiente rem commodi dicta magni provident est excepturi! Temporibus blanditiis, asperiores porro!</p>
					</div>
				</div>
			</li>
			<li class="item">
				<a href="#compatibilizacao-com-os-projetos-complementares" class="service-link" title="">
					<div class="service-item">
						<div>
							<h3 class="section-title icon-puzzle icon-top">Compatibilização com os Projetos Complementares</h3>
						</div>
					</div>
				</a>
				<div class="service-content" id="compatibilizacao-com-os-projetos-complementares">
					<a href="#" title="" class="close">fechar</a>
					<div class="container">
						<h3 class="section-title icon-puzzle icon-left">Compatibilização com os Projetos Complementares</h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quam maiores soluta praesentium iure magni neque, facilis perspiciatis repellendus consequuntur laborum obcaecati, nesciunt aliquid animi laudantium provident, possimus, doloremque recusandae voluptatem.</p>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Libero veritatis saepe, non nemo, illum quibusdam facere provident, molestiae esse aliquid incidunt minus similique consectetur iure labore ex odio, id ea.</p>
					</div>
				</div>
			</li>
			<li class="item">
				<a href="#projeto-e-consultoria-de-iluminacao" class="service-link" title="">
					<div class="service-item">
						<div>
							<h3 class="section-title icon-light icon-top">Projeto e Consultoria de iluminação</h3>
						</div>
					</div>
				</a>
				<div class="service-content" id="projeto-e-consultoria-de-iluminacao">
					<a href="#" title="" class="close">fechar</a>
					<div class="container">
						<h3 class="section-title icon-light icon-left">Projeto e Consultoria de iluminação</h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facere magni distinctio sint architecto illo est eos minus culpa sapiente harum illum ipsa, delectus, quos provident et cumque. Dolorum, odit, a.</p>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eum quo corporis impedit doloribus veritatis repellendus ex, nulla blanditiis vel tempora earum. Odio praesentium eius consectetur repudiandae, doloremque. Ullam fuga, ad?</p>
					</div>
				</div>
			</li>
			<li class="item">
				<a href="#estudo-de-viabilidade-do-terreno1" class="service-link" title="">
					<div class="service-item">
						<div>
							<h3 class="section-title icon-search icon-top">Estudo de viabilidade do terreno</h3>
						</div>
					</div>
				</a>
				<div class="service-content" id="estudo-de-viabilidade-do-terreno1">
					<a href="#" title="" class="close">fechar</a>
					<div class="container">
						<h3 class="section-title icon-search icon-left">Estudo de viabilidade do terreno</h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Harum laudantium, soluta ducimus dolorum ipsum hic at? Reiciendis mollitia saepe consequatur, veritatis sed sint. Eveniet sunt, dolore, velit provident voluptates quos.</p>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Temporibus quod ipsa doloremque enim debitis atque dolorum minima numquam labore perferendis, ut veniam nesciunt eligendi eveniet veritatis id magni quis nemo.</p>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Harum laudantium, soluta ducimus dolorum ipsum hic at? Reiciendis mollitia saepe consequatur, veritatis sed sint. Eveniet sunt, dolore, velit provident voluptates quos.</p>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Temporibus quod ipsa doloremque enim debitis atque dolorum minima numquam labore perferendis, ut veniam nesciunt eligendi eveniet veritatis id magni quis nemo.</p>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Harum laudantium, soluta ducimus dolorum ipsum hic at? Reiciendis mollitia saepe consequatur, veritatis sed sint. Eveniet sunt, dolore, velit provident voluptates quos.</p>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Temporibus quod ipsa doloremque enim debitis atque dolorum minima numquam labore perferendis, ut veniam nesciunt eligendi eveniet veritatis id magni quis nemo.</p>
					</div>
				</div>
			</li>
			<li class="item">
				<a href="#projeto-de-arquitetura1" class="service-link" title="">
					<div class="service-item">
						<div>
							<h3 class="section-title icon-project icon-top">Projeto de Arquitetura</h3>
						</div>
					</div>
				</a>
				<div class="service-content" id="projeto-de-arquitetura1">
					<a href="#" title="" class="close">fechar</a>
					<div class="container">
						<h3 class="section-title icon-project icon-left">Projeto de Arquitetura</h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tempore numquam modi, aut nulla quod praesentium. Nesciunt magnam, fugiat deserunt esse. Molestiae ad, repellendus. Quibusdam minima ea velit, corrupti voluptates rerum!</p>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci deleniti laudantium qui accusantium ex, quibusdam beatae, nesciunt necessitatibus sapiente rem commodi dicta magni provident est excepturi! Temporibus blanditiis, asperiores porro!</p>
					</div>
				</div>
			</li>
			<li class="item">
				<a href="#compatibilizacao-com-os-projetos-complementares1" class="service-link" title="">
					<div class="service-item">
						<div>
							<h3 class="section-title icon-puzzle icon-top">Compatibilização com os Projetos Complementares</h3>
						</div>
					</div>
				</a>
				<div class="service-content" id="compatibilizacao-com-os-projetos-complementares1">
					<a href="#" title="" class="close">fechar</a>
					<div class="container">
						<h3 class="section-title icon-puzzle icon-left">Compatibilização com os Projetos Complementares</h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quam maiores soluta praesentium iure magni neque, facilis perspiciatis repellendus consequuntur laborum obcaecati, nesciunt aliquid animi laudantium provident, possimus, doloremque recusandae voluptatem.</p>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Libero veritatis saepe, non nemo, illum quibusdam facere provident, molestiae esse aliquid incidunt minus similique consectetur iure labore ex odio, id ea.</p>
					</div>
				</div>
			</li>
			<li class="item">
				<a href="#projeto-e-consultoria-de-iluminacao1" class="service-link" title="">
					<div class="service-item">
						<div>
							<h3 class="section-title icon-light icon-top">Projeto e Consultoria de iluminação</h3>
						</div>
					</div>
				</a>
				<div class="service-content" id="projeto-e-consultoria-de-iluminacao1">
					<a href="#" title="" class="close">fechar</a>
					<div class="container">
						<h3 class="section-title icon-light icon-left">Projeto e Consultoria de iluminação</h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facere magni distinctio sint architecto illo est eos minus culpa sapiente harum illum ipsa, delectus, quos provident et cumque. Dolorum, odit, a.</p>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eum quo corporis impedit doloribus veritatis repellendus ex, nulla blanditiis vel tempora earum. Odio praesentium eius consectetur repudiandae, doloremque. Ullam fuga, ad?</p>
					</div>
				</div>
			</li>
		</ul>
	</section>
	<section class="portfolio" id="portfolio">
		<div class="container clearfix">
			<h2>Portfolio</h2>
			<h3 class="sub-title open-cat-menu">Tipos de projeto</h3>
			<ul class="cat-menu">
				<li class="cat-menu-item"><a href="" title="" class="cat-menu-link has-submenu" data-cat="1">Comerciais</a>
					<ul class="submenu">
						<li class="submenu-item"><a href="" class="submenu-link" data-cat="1-1">Estudo de viabilidade do terreno</a></li>
						<li class="submenu-item"><a href="" class="submenu-link" data-cat="1-2">Compatibilização de Projetos Complementares</a></li>
						<li class="submenu-item"><a href="" class="submenu-link" data-cat="1-3">Projeto de Arquitetura</a></li>
						<li class="submenu-item"><a href="" class="submenu-link" data-cat="1-4">Consultoria em projeto de iluminação</a></li>
					</ul>
				</li>
				<li class="cat-menu-item"><a href="" title="" class="cat-menu-link has-submenu" data-cat="2">Residenciais</a>
					<ul class="submenu">
						<li class="submenu-item"><a href="" class="submenu-link">Estudo de viabilidade do terreno</a></li>
						<li class="submenu-item"><a href="" class="submenu-link">Compatibilização de Projetos Complementares</a></li>
						<li class="submenu-item"><a href="" class="submenu-link">Projeto de Arquitetura</a></li>
						<li class="submenu-item"><a href="" class="submenu-link">Consultoria em projeto de iluminação</a></li>
					</ul>
				</li>
				<li class="cat-menu-item"><a href="" title="" class="cat-menu-link" data-cat="3">Especiais</a></li>
				<li class="cat-menu-item cat-menu-item--current"><a href="" title="" class="cat-menu-link" data-cat="todos">Todos</a></li>
			</ul>
		</div>
		<div class="portfolio-items">
			<a href="fasdf" class="portfolio-item cat-2" style="background-image: url(images/temp/portfolio-2.jpg);">
				<div class="portfolio-mask">
					<div class="portfolio-info">
						<h3 class="sub-title">Saint Lorence Residence</h3>
						<p>Projeto Fachada</p>
					</div>
				</div>
			</a>
			<a href="fadf" class="portfolio-item cat-1 cat-1-1" style="background-image: url(images/temp/portfolio-2.jpg);">
				<div class="portfolio-mask">
					<div class="portfolio-info">
						<h3 class="sub-title">Saint Lorence Residence</h3>
						<p>Projeto Fachada</p>
					</div>
				</div>
			</a>
			<a href="fadf" class="portfolio-item vertical cat-2" style="background-image: url(images/temp/portfolio.jpg);">
				<div class="portfolio-mask">
					<div class="portfolio-info">
						<h3 class="sub-title">Saint Lorence Residence</h3>
						<p>Projeto Fachada</p>
					</div>
				</div>
			</a>
			<a href="fadf" class="portfolio-item vertical cat-1 cat-1-2" style="background-image: url(images/temp/portfolio.jpg);">
				<div class="portfolio-mask">
					<div class="portfolio-info">
						<h3 class="sub-title">Saint Lorence Residence</h3>
						<p>Projeto Fachada</p>
					</div>
				</div>
			</a>
			<a href="fadf" class="portfolio-item vertical cat-1 cat-1-1" style="background-image: url(images/temp/portfolio.jpg);">
				<div class="portfolio-mask">
					<div class="portfolio-info">
						<h3 class="sub-title">Saint Lorence Residence</h3>
						<p>Projeto Fachada</p>
					</div>
				</div>
			</a>
			<a href="fadf" class="portfolio-item cat-1 cat-1-3" style="background-image: url(images/temp/portfolio-2.jpg);">
				<div class="portfolio-mask">
					<div class="portfolio-info">
						<h3 class="sub-title">Saint Lorence Residence</h3>
						<p>Projeto Fachada</p>
					</div>
				</div>
			</a>
			<a href="fadf" class="portfolio-item vertical cat-2" style="background-image: url(images/temp/portfolio.jpg);">
				<div class="portfolio-mask">
					<div class="portfolio-info">
						<h3 class="sub-title">Saint Lorence Residence</h3>
						<p>Projeto Fachada</p>
					</div>
				</div>
			</a>
			<a href="fadf" class="portfolio-item cat-2" style="background-image: url(images/temp/portfolio-2.jpg);">
				<div class="portfolio-mask">
					<div class="portfolio-info">
						<h3 class="sub-title">Saint Lorence Residence</h3>
						<p>Projeto Fachada</p>
					</div>
				</div>
			</a>
			<a href="fadf" class="portfolio-item vertical cat-2" style="background-image: url(images/temp/portfolio.jpg);">
				<div class="portfolio-mask">
					<div class="portfolio-info">
						<h3 class="sub-title">Saint Lorence Residence</h3>
						<p>Projeto Fachada</p>
					</div>
				</div>
			</a>
			<a href="fadf" class="portfolio-item cat-2" style="background-image: url(images/temp/portfolio-2.jpg);">
				<div class="portfolio-mask">
					<div class="portfolio-info">
						<h3 class="sub-title">Saint Lorence Residence</h3>
						<p>Projeto Fachada</p>
					</div>
				</div>
			</a>
			<a href="fadf" class="portfolio-item cat-1 cat-1-4" style="background-image: url(images/temp/portfolio-2.jpg);">
				<div class="portfolio-mask">
					<div class="portfolio-info">
						<h3 class="sub-title">Saint Lorence Residence</h3>
						<p>Projeto Fachada</p>
					</div>
				</div>
			</a>
		</div>
	</section>
	<section class="contact" id="contact">
		<div class="container">
			<h2><span>Entre em</span> contato <span>conosco</span></h2>
			<form action="" method="">
				<div class="wpcf7-response-output wpcf7-mail-sent-ok">mensagem enviada! em breve entraremos em contato</div>
				<div class="wpcf7-response-output wpcf7-validation-errors">corrija os itens destacados abaixo</div>
				<div class="one-third alpha">
					<label class="error">Nome*</label>
					<input type="text" name="" value="" placeholder="" class="wpcf7-not-valid">
				</div>
				<div class="five-twelfths">
					<label>E-mail</label>
					<input type="text" name="" value="" placeholder="">
				</div>
				<div class="one-quarter omega te">
					<label>Telefone</label>
					<input type="text" name="" value="" placeholder="">
				</div>
				<div class="clear"></div>
				<label for="">Mensagem</label>
				<textarea name=""></textarea>
				<input type="button" name="" value="Enviar" class="button">
			</form>
		</div>
		<div class="contacts clearfix">
			<div class="container">
				<div class="one-third">
					<div class="contact-info icon-phone">
						<p>3922 4056</p>
						<p class="contact-name">Escritório</p>
					</div>
				</div>
				<div class="one-third">
					<div class="contact-info icon-mobile">
						<p>3922 4056</p>
						<p class="contact-name">Escritório</p>
					</div>
				</div>
				<div class="one-third">
					<div class="contact-info icon-mobile">
						<p>3922 4056</p>
						<p class="contact-name">Escritório</p>
					</div>
				</div>
			</div>
		</div>
		<div class="clear"></div>
		<address>Rua C104 Qd 19 Lt 15 Jd América - Goiânia GO<br><a href="https://www.google.com/maps?ll=-16.71574,-49.277434&z=15&hl=en-US&gl=US&mapclient=apiv3" title="abrir localização" target="-blank">abrir localização</a></address>
		<div id="map_canvas" class="map">
		</div>
	</section>
	<section class="clients">
		<div class="container">
			<h2>Clientes</h2>
			<div class="clients-carousel">
				<div class="carousel-item"><img src="images/temp/client.png" alt=""></div>
				<div class="carousel-item"><img src="images/temp/client2.png" alt=""></div>
				<div class="carousel-item"><img src="images/temp/client3.png" alt=""></div>
				<div class="carousel-item"><img src="images/temp/client.png" alt=""></div>
				<div class="carousel-item"><img src="images/temp/client2.png" alt=""></div>
				<div class="carousel-item"><img src="images/temp/client3.png" alt=""></div>
				<div class="carousel-item"><img src="images/temp/client.png" alt=""></div>
				<div class="carousel-item"><img src="images/temp/client2.png" alt=""></div>
				<div class="carousel-item"><img src="images/temp/client3.png" alt=""></div>
				<div class="carousel-item"><img src="images/temp/client.png" alt=""></div>
				<div class="carousel-item"><img src="images/temp/client2.png" alt=""></div>
				<div class="carousel-item"><img src="images/temp/client3.png" alt=""></div>
				<div class="carousel-item"><img src="images/temp/client.png" alt=""></div>
				<div class="carousel-item"><img src="images/temp/client2.png" alt=""></div>
				<div class="carousel-item"><img src="images/temp/client3.png" alt=""></div>
				<div class="carousel-item"><img src="images/temp/client.png" alt=""></div>
			</div>
		</div>
	</section>
	<div class="clear"></div>
</div>
</div>
<?php include "_footer.php" ?>